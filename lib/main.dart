import 'package:flutter/material.dart';
import 'dart:core';
import 'dart:async';

void main() {
  runApp(WeatherApp());
}

class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Weather App',
      home: WeatherScreen(),
    );
  }
}

class WeatherScreen extends StatefulWidget {
  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/bg.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            alignment: Alignment(0, -0.8),
            child: Text(
              '${getDayOfWeekString(DateTime.now().weekday)}',
              style: TextStyle(
                fontSize: 30.0,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
              alignment: Alignment(0, -0.5),
              child: Text(
                '31°C',
                style: TextStyle(
                  fontSize: 130.0,
                  color: Colors.white,
                ),
                textAlign: TextAlign.center,
              ),
          ),
          Container(
            alignment: Alignment(0, -0.1),
            child: Text(
              'Chonburi',
              style: TextStyle(
                fontSize: 35.0,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            alignment: Alignment(0, 0.5),
            child: Icon(
              Icons.sunny,
              size: 300.0,
              color: Colors.white,
            ),
          ),
          Container(
            alignment: Alignment(0, 0.8),
            child: Text(
              '${DateTime.now().hour}:${DateTime.now().minute}:${DateTime.now().second}',
              style: TextStyle(
                fontSize: 35.0,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      )
    );
  }
}

String getDayOfWeekString(int dayOfWeek) {
  switch (dayOfWeek) {
    case 1:
      return 'Monday';
    case 2:
      return 'Tuesday';
    case 3:
      return 'Wednesday';
    case 4:
      return 'Thursday';
    case 5:
      return 'Friday';
    case 6:
      return 'Saturday';
    case 7:
      return 'Sunday';
    default:
      return 'Unknown';
  }
}